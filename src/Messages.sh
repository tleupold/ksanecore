# SPDX-FileCopyrightText: None
# SPDX-License-Identifier: CC0-1.0

#! /bin/sh

$XGETTEXT `find . -name \*.cpp` -o $podir/ksanecore.pot

